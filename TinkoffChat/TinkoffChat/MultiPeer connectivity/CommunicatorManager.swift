//
//  CommunicatorManager.swift
//  TinkoffChat
//
//  Created by Dmitry on 09/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class CommunicatorManager: NSObject, CommutatorDelegate {

    let multipeer = MultipeerCommunicator()
    weak var delegateConversationModel:IConversationModel?
    weak var delegationMessageChat:chatDelegate?
    
    override init() {
        super.init()
        multipeer.delegate = self
    }
    
    
    
    //MARK: CommutatorDelegate 
    func didFoundUser(userID: String, userName : String?) {
        
        delegateConversationModel?.newOnlineUser(userID: userID, userName: userName)
        
    }
    
    
    
    func didLostUser(userID: String) {
        
        delegateConversationModel?.userGoOffline(userId: userID)
        
    }
 

    //errors
    func failedToStarBrowsingForUsers(error: Error) {
        print(error)
        
    }
    
    func failedToStartAdvertising(error: Error) {
        print(error)
        
    }
    
    
    
    
    //messages
    func didReceiveMessage(text: String, fromUser: String, toUser: String) {
        
        delegateConversationModel?.newMessageCome(text: text, fromUser: fromUser, toUser: toUser)
 
    }
    
    func messageSend(text: String, toUser: String) {
        
        multipeer.sendMessage(string: text, to: toUser, completionHandler: nil)
        
        
    }
   
    
    
    
}
