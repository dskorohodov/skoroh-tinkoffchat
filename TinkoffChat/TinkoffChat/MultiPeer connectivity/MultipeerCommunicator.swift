//
//  MultipeerCommunicator.swift
//  TinkoffChat
//
//  Created by Dmitry on 09/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit
import MultipeerConnectivity


let myOwnUserId = UIDevice.current.identifierForVendor?.uuidString ?? "without uuid"


protocol Commutator {
    func sendMessage(string: String, to userID: String, completionHandler : ((_ success : Bool, _ error: Error?) -> ())?)
    weak var delegate : CommutatorDelegate? {get set}
    var online : Bool {get set}
}


class MultipeerCommunicator: NSObject, MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate, Commutator {
    
    
    //test
    var dictPeersInSession = [MCPeerID:MCSession]() //control session list

    var dictPeerIdUserName = [MCPeerID:String]() 
    
    var dictUserIdPeer = [String:MCPeerID]()
    
    
    

    private let chatServiceType = "tinkoff-chat"
    
    private let myPeerId = MCPeerID(displayName: myOwnUserId)
    private let myDiscoveryInfo = ["userName": "sayrong"]
    
    private let serviceAdvertiser: MCNearbyServiceAdvertiser
    private let serviceBrowser: MCNearbyServiceBrowser
    
    
    
    override init() {
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: myDiscoveryInfo, serviceType: chatServiceType)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: chatServiceType)
        self.online = true
        
        super.init()
        self.serviceAdvertiser.delegate = self
        self.serviceAdvertiser.startAdvertisingPeer()
        
        self.serviceBrowser.delegate = self
        self.serviceBrowser.startBrowsingForPeers()
       
    }
    
    
    
    deinit {
        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
    }
    
    
    // MARK: MCNearbyServiceAdvertiserDelegate
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        NSLog("Failed to Advertising Peers - \(error)")
        delegate?.failedToStartAdvertising(error: error)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        
        var contextString = "No context"
        if (context != nil) {
            if let unwrapperString = String(data: context!, encoding: String.Encoding.utf8) {
                contextString = unwrapperString
            }
        }
        NSLog("Receved Invitation from \(peerID) with context \(contextString)")
        
        
        if (dictPeersInSession[peerID] == nil) {
            let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .none)
            session.delegate = self
            dictPeersInSession[peerID] = session
            
            invitationHandler(true, session)

        } else {
            invitationHandler(true, dictPeersInSession[peerID])
        }
        
    }
    
    
    //MARK: MCNearbyServiceBrowserDelegate 
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        NSLog("Failed to start Browsing - \(error)")
        delegate?.failedToStarBrowsingForUsers(error: error)
    }
    
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        var userName: String = "nil"
        
        if (info != nil) {
            if let unwrappedUserName = info.map({$0["userName"]}) {
                userName  = unwrappedUserName!
                
                dictPeerIdUserName[peerID] = userName
                dictUserIdPeer[peerID.displayName] = peerID
                
                
                if (dictPeersInSession[peerID] == nil) {
                    
                    let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.none)
                    session.delegate = self
                    
                    dictPeersInSession[peerID] = session
                    
                    browser.invitePeer(peerID, to: session, withContext: nil, timeout: 30)
                } else {
                    browser.invitePeer(peerID, to: dictPeersInSession[peerID]!, withContext: nil, timeout: 15)
                }

                
            } else {
                NSLog("Wrong DiscoveryInfo - \(info!)")
            }
        }
        
        NSLog("***********************")
        NSLog("Found peer - \(peerID) with discoveryInfo - \(userName)")
        NSLog("***********************")
        
        
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        NSLog("Lost peer - \(peerID)")
        
    }
    
    
    
    //MARK: MCSessionDelegate
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
       
        NSLog("\nSession changed state with \(peerID) to")
        switch state {
        case .connected:
            print("Connected")
            
            let userName = dictPeerIdUserName[peerID]
            delegate?.didFoundUser(userID: peerID.displayName, userName: userName)
    
        case .connecting:
            print("Connecting")
        case .notConnected:
            print("not Connected")

            delegate?.didLostUser(userID: peerID.displayName)
            session.cancelConnectPeer(peerID)
            dictPeersInSession.removeValue(forKey: peerID)
        }
        
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        NSLog("\nDid receive data from \(peerID) - \(data)")
        
        var receivedMessage: String
        
        if let message = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: String] {
            if (message["text"] != nil) {
                receivedMessage = message["text"]!
                
                delegate?.didReceiveMessage(text: receivedMessage, fromUser: peerID.displayName, toUser: myPeerId.displayName)
            }
        }
        
        
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        NSLog("\nDid recieve stream with name \(streamName) from \(peerID)")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        NSLog("\nDid start receive Resource with name \(resourceName) from peer \(peerID)")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) {
        NSLog("\nFinish receicing Resource with name \(resourceName) from \(peerID)")
    }
    
    
    
    
    //MARK: Commutator protocol

    
    var online: Bool
    weak var delegate: CommutatorDelegate?
    
    
    func generateMessageId() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }
    
    
    func sendMessage(string: String, to userID: String, completionHandler : ((_ success: Bool, _ error : Error?) -> ())?) {
        
        let id = generateMessageId()
        let message = ["eventType":"textMessage", "messageId": id,"text": string]
        
        let ourPeerId = dictUserIdPeer[userID]
        let ourSession = dictPeersInSession[ourPeerId!]
        
        
        if let theJSONdata = try? JSONSerialization.data(withJSONObject: message, options: .prettyPrinted) {
            do {
                try ourSession?.send(theJSONdata, toPeers:[ourPeerId!] , with: .unreliable)
            } catch {
                print("Error")
            }
        }
       
  
    }
    
    
    

}
