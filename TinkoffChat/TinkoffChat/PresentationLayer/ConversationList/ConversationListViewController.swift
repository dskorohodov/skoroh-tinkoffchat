//
//  ConversationListViewController.swift
//  TinkoffChat
//
//  Created by Dmitry on 25/03/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

protocol IConversationModel: class {
    weak var delegateConversationList: IConversationListViewDelegate? {get set}
    weak var delegateChat: chatDelegate? {get set}
    func newOnlineUser(userID: String, userName : String?)
    func userGoOffline(userId: String)
    func newMessageCome(text: String, fromUser: String, toUser: String)
    func newMessageSend(text: String, toUser: String)
}


class ConversationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, IConversationListViewDelegate {


    @IBOutlet weak var chatTableView: UITableView!
    
    let model = ConversationListModel()
    
    var conversationTitle: String!
    var selectedUserId: String!
    var selectedUserMessages: [Message] = []
    
    private var dataSource: [[Conversation]] = [[],[]]
    
    
    func setup(dataSource:[[Conversation]]) {
        self.dataSource = dataSource
        DispatchQueue.main.async {
            self.chatTableView.reloadData()
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        model.delegateConversationList = self
        
        self.chatTableView.dataSource = self
        self.chatTableView.delegate = self
        // Do any additional setup after loading the view.
    
    
    }

    

    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return dataSource[0].count
        } else {
            return dataSource[1].count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Online" : "History"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "conversationListCell", for: indexPath) as! ConversationListCell
        
        
        cell.name = dataSource[indexPath.section][indexPath.row].name
        cell.message = dataSource[indexPath.section][indexPath.row].messages.last?.textMessage
        cell.date = dataSource[indexPath.section][indexPath.row].messages.last?.dateMessage
        cell.online = dataSource[indexPath.section][indexPath.row].online
        cell.hasUnreadMessages = dataSource[indexPath.section][indexPath.row].hasUnreadMessages
        
        
        cell.configureCell()
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toConversation") {
            let myNextVC = (segue.destination as! ConversationViewController)
            myNextVC.title = conversationTitle
            myNextVC.userId = selectedUserId
            myNextVC.messageArray = selectedUserMessages
            model.delegateChat = myNextVC
            myNextVC.delegateModel = model
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            conversationTitle = dataSource[indexPath.section][indexPath.row].name
            selectedUserId = dataSource[indexPath.section][indexPath.row].userId
            selectedUserMessages = dataSource[indexPath.section][indexPath.row].messages
            performSegue(withIdentifier: "toConversation", sender: self)
            tableView .deselectRow(at: indexPath, animated: true)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
