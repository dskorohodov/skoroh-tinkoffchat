//
//  Message.swift
//  TinkoffChat
//
//  Created by Dmitry on 22/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class Message: NSObject {
    var textMessage: String
    var dateMessage: Date
    var fromUserId: String
    var toUserId: String
    let income: Bool
    
    
    init(text: String, date: Date, fromUserId: String, toUserId: String, incomeMsg: Bool) {
        textMessage = text
        dateMessage = date
        self.fromUserId = fromUserId
        self.toUserId = toUserId
        self.income = incomeMsg
    }
    
}
