//
//  ConversationCell.swift
//  TinkoffChat
//
//  Created by Dmitry on 25/03/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit


protocol ConversationCellConfiguration: class {
    var name: String? {get set}
    var message: String? {get set}
    var date: Date? {get set}
    var online: Bool {get set}
    var hasUnreadMessages: Bool {get set}
}


class ConversationListCell: UITableViewCell, ConversationCellConfiguration {


    // MARK: IBOutlets 
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var name: String?
    var message: String?
    var date: Date?
    var online: Bool = false
    var hasUnreadMessages: Bool = false

    
    func isToday(date: Date) -> Bool {
        return NSCalendar.current.isDateInToday(date)
    }
    

    func configureCell() {
        nameLabel.text = self.name
        messageLabel.text = self.message ?? "No messages yet"
        
        
        if let unwrappedDate = self.date {
            let dateFormatter = DateFormatter()
            if(isToday(date: unwrappedDate)) {
                dateFormatter.dateFormat = "HH:mm"
            } else {
                dateFormatter.dateFormat = "dd MMM"
            }
            
            let stingOfDate = dateFormatter.string(from: unwrappedDate)
            dateLabel.text = stingOfDate
        }
        
    
        if (self.online == true) {
            self.backgroundColor = UIColor(red: 249/255, green: 255/255, blue: 188/255, alpha: 1)
        } else {
            self.backgroundColor = UIColor.white
        }
        
        if (self.hasUnreadMessages == true) {
            self.messageLabel.font = UIFont.boldSystemFont(ofSize: 17)
        } else {
            self.messageLabel.font = UIFont.systemFont(ofSize: 16)
        }
        
        
        if (self.message == nil) {
            dateLabel.text = ""
            self.messageLabel.font = UIFont(name: "Arial", size: 14)
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
