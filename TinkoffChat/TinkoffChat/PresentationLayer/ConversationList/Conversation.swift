//
//  Conversation.swift
//  TinkoffChat
//
//  Created by Dmitry on 22/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class Conversation: NSObject {
    var userId: String
    var name: String?
    var messages: [Message]
    var online: Bool!
    var hasUnreadMessages: Bool!
    
    init(userId: String, name: String?, online: Bool!,  hasUnread: Bool! ) {
        
        self.userId = userId
        self.name = name
        self.messages = []
        self.online = online
        self.hasUnreadMessages = hasUnread
        
    }
}
