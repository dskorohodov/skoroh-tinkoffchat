//
//  ConversationListModel.swift
//  TinkoffChat
//
//  Created by Dmitry on 22/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit




protocol IConversationListViewDelegate: class {
    func setup(dataSource:[[Conversation]])
}


// MARK: Service Protocol
protocol CommutatorDelegate : class {
    //discovering
    func didFoundUser(userID: String, userName : String?)
    func didLostUser(userID: String)
    
    //errors
    func failedToStarBrowsingForUsers(error: Error)
    func failedToStartAdvertising(error: Error)
    
    //messages
    func didReceiveMessage(text: String, fromUser: String, toUser: String)
    
}


class ConversationListModel: IConversationModel  {
    
    
    let manager = CommunicatorManager()
    
    var allConversations:[Conversation] = []
    var structedDataSource: [[Conversation]] = []
    
    
    var delegateConversationList: IConversationListViewDelegate?
    var delegateChat: chatDelegate?
    
    init() {
        manager.delegateConversationModel = self
    }
    
    
    func newOnlineUser(userID: String, userName : String?) {
        
        var userExists = false
        
        for currentConversation in allConversations {
            if (currentConversation.userId == userID) {
                userExists = true
                currentConversation.online = true
            }
        }
        
        if (!userExists) {
            var newConversation = Conversation(userId: userID, name: userName ??  "without Name", online: true, hasUnread: false)
            self.allConversations.append(newConversation)
        }
    
        let dataTableView = configDataSource()
        
        delegateConversationList?.setup(dataSource: dataTableView)
        
    }
    
    
    func userGoOffline(userId: String) {
        
        for currentConversation in allConversations {
            if (currentConversation.userId == userId) {
                currentConversation.online = false
                
                let dataTableView = configDataSource()
                delegateConversationList?.setup(dataSource: dataTableView)
                
            }
        }
        
        
    }
    
    func newMessageCome(text: String, fromUser: String, toUser: String) {
        
        let newMessage = Message(text: text, date: Date(), fromUserId: fromUser, toUserId: toUser, incomeMsg: true)
        
        for currentConversation in allConversations {
            if (currentConversation.userId == fromUser){
                currentConversation.messages.append(newMessage)
            }
        }
        
        let dataTableView = configDataSource()
        delegateConversationList?.setup(dataSource: dataTableView)
        delegateChat?.newMessage(text: text, income: true)
        
    }
    
    
    func newMessageSend(text: String, toUser: String) {
        let newMessage = Message(text: text, date: Date(), fromUserId: myOwnUserId, toUserId: toUser, incomeMsg: false)
        
        for currentConversation in allConversations {
            if (currentConversation.userId == toUser){
                currentConversation.messages.append(newMessage)
            }
        }

        manager.messageSend(text: text, toUser: toUser)
        delegateChat?.newMessage(text: text, income: false)
        
        let dataTableView = configDataSource()
        delegateConversationList?.setup(dataSource: dataTableView)
    }
    
    
    
    func configDataSource() -> [[Conversation]] {
        
        var onlineConversation:[Conversation] = []
        var offlineConversation:[Conversation] = []
        var result: [[Conversation]] = []
        
        for currentConversation in allConversations {
            if currentConversation.online == true {
                onlineConversation.append(currentConversation)
            } else {
                offlineConversation.append(currentConversation)
            }
        }
        
        result.append(onlineConversation)
        result.append(offlineConversation)
        
        return result
        
    }
    
    
    

}
