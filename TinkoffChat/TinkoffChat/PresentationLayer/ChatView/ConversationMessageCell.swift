//
//  ConversationMessageCellTableViewCell.swift
//  TinkoffChat
//
//  Created by Dmitry on 26/03/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

protocol MessageCellConfiguration {
    var textMessage : String? {get set}
}

class ConversationMessageCell: UITableViewCell, MessageCellConfiguration {

    
    @IBOutlet weak var messageLabel: UILabel!
    
    
    var textMessage : String?
    
    func configCell(income:Bool) {
        if let unwrapedText = textMessage {
            messageLabel.numberOfLines = 0
            
            //messageLabel.sizeToFit()
            messageLabel.text = unwrapedText
            messageLabel.layer.masksToBounds = true
            messageLabel.layer.cornerRadius = 5
        }
        
        if (income == true) {
            messageLabel.backgroundColor = UIColor (red: 230/255, green: 230/255, blue: 235/255, alpha: 1)
        } else {
            messageLabel.backgroundColor = UIColor (red: 81/255, green: 158/255, blue: 255/255, alpha: 1)
            messageLabel.textColor = UIColor.white
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //        var backgroundImage = UIImageView(image: UIImage(named: "star"))
        //        backgroundImage.contentMode = UIViewContentMode.ScaleAspectFit
        //        self.backgroundView = backgroundImage
    }
    
    
}
