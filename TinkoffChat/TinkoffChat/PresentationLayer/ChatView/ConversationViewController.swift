//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by Dmitry on 26/03/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

struct message {
    let text: String!
    let income: Bool
    
    init(text: String!, income: Bool) {
        self.text = text
        self.income = income
    }
}

protocol chatDelegate: class {
    func newMessage(text: String, income: Bool)
}


class ConversationViewController: UIViewController, UITableViewDataSource, UITextFieldDelegate, chatDelegate {

    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    
    
    var messageArray = [Message]()
    var userId: String?
    
    weak var delegateModel: IConversationModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.messageTextField.delegate = self
        self.messageTableView.dataSource = self
        self.messageTableView.estimatedRowHeight = 60
        self.messageTableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.

        
    }
    
    
    func newMessage(text: String, income: Bool) {
        let myMessage = Message(text: text, date: Date(), fromUserId:myOwnUserId , toUserId: userId!, incomeMsg: income)
        messageArray.append(myMessage)
        DispatchQueue.main.async {
            self.messageTableView.reloadData()
        }
        
    }
    

    @IBAction func sendMessageButton(_ sender: UIButton) {
        
        if let unwrapedMessage = messageTextField.text {
            
            delegateModel?.newMessageSend(text: unwrapedMessage, toUser: userId!)
            
            self.messageTextField.text = ""
            
        }
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UITableViewDataSource 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  messageArray.count
    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        let cell = tableView.dequeueReusableCell(withIdentifier: messageArray[indexPath.row].income ? "conversationCellFrom" : "conversationCellTo", for: indexPath) as! ConversationMessageCell
        
        cell.textMessage = messageArray[indexPath.row].textMessage
        
        cell.configCell(income: messageArray[indexPath.row].income)
 
        return cell
    }
    
    // MARK: - UITextFieldDelegate
    // TODO: Need do move only textfield frame with keyboard
    
    var keyboardActive = false
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if  keyboardActive == false {
            self.view.frame.origin.y -= 250
            self.keyboardActive = true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if  keyboardActive == true {
            self.view.frame.origin.y += 250
            self.keyboardActive = false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
