//
//  OperationDataManager.swift
//  TinkoffChat
//
//  Created by Dmitry on 03/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class OperationDataManager: Operation {
    
    
    var savedSettings: UserProfileSettings
    var newSettings = UserProfileSettings(userName: nil, textAboutMe: nil, userPhoto: nil, textColor: nil)
    var smthGoesWrong: Bool = false
    let fileName = "savedUserSettings"
    
    
    var toSave: Bool
    
    init(name: String?, aboutMe: String?, userPhoto: UIImage?, textColor: UIColor?, save: Bool)  {
        
        savedSettings = UserProfileSettings(userName: name, textAboutMe: aboutMe, userPhoto: userPhoto, textColor: textColor)
        toSave = save
        
        
    }
 
    override func main() {
        
        if self.isCancelled {
            return
        }
        
        
        if (toSave) {
            
            let dataToWrite = NSKeyedArchiver.archivedData(withRootObject: savedSettings)
            
            savePhoto(Photo: dataToWrite)
            
            print("Успех")
            
        } else {
            
            let dataFromFile = read()
            if let unwarapped = dataFromFile {
                newSettings = NSKeyedUnarchiver.unarchiveObject(with: unwarapped) as! UserProfileSettings
                
            }
        }
        
    }
    
    
    
    
    func savePhoto(Photo: Data) {
        
        let fileManager = FileManager.default
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(fileName)
            print("\(path)")
            
            if (fileManager.fileExists(atPath: path.path)) {
                
                print("Файл уже создан")
                
                do {
                    let content = try Data(contentsOf: path)
                    
                    if (content != Photo) {
                        do {
                            try Photo.write(to: path, options: .atomic)
                        } catch {
                            print("Ошибка при записи файла \(error)")
                            smthGoesWrong = true
                        }
                    } else {
                        print("Фото не изменилось")
                    }
                    
                } catch {
                    print("Ошибка при чтении файла \(error)")
                    smthGoesWrong = true
                }
                
            } else {
                
                do {
                    try Photo.write(to: path, options: .atomic)
                    print("Файл создается впервые")
                } catch {
                    print("Ошибка при записи файла \(error)")
                    smthGoesWrong = true
                }
            }
        }
        
        
    }
    
    
    func read() -> Data? {
        
        if  let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(fileName)
            
            do {
                let data = try Data(contentsOf: path)
                return data
            } catch {
                print("Error here")
                smthGoesWrong = true
                return nil
            }
            
        }
        
        return nil
    }
    
    
}
