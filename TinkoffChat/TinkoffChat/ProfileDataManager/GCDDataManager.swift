//
//  GCDDataManager.swift
//  TinkoffChat
//
//  Created by Dmitry on 01/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class GCDDataManager: NSObject {
    
    
    
    
    let globalQueue = DispatchQueue.global(qos: .utility)
    
    let fileName = "savedUserSettings"
    var smthGoesWrong: Bool = false
    
    
    
    func writeWithGCD (name: String?, aboutMe: String?, userPhoto: UIImage?, textColor: UIColor?, vc: ViewController) {
        
        let settingsToSave = UserProfileSettings(userName: name, textAboutMe: aboutMe, userPhoto: userPhoto, textColor: textColor)
        
        globalQueue.async {
            
            let dataToSave = NSKeyedArchiver.archivedData(withRootObject: settingsToSave)
            self.savaDataToFile(userData: dataToSave)
            
            DispatchQueue.main.async {
                
                vc.stopAnimate()
                
                if (!self.smthGoesWrong) {
                    vc.alertSuccessDataLoad()
                } else {
                    vc.alertErrorDataLoad(GCD: true)
                }
                
                
            }
            
            
        }
        
        
    }
    
    
    
    func savaDataToFile(userData: Data) {
        
        let fileManager = FileManager.default
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(self.fileName)
            print("\(path)")
            
            if (fileManager.fileExists(atPath: path.path)) {
                
                print("Файл уже создан")
                
                do {
                    let content = try Data(contentsOf: path)
                    
                    if (content != userData) {
                        do {
                            try userData.write(to: path, options: .atomic)
                        } catch {
                            print("Ошибка при записи файла \(error)")
                            smthGoesWrong = true
                        }
                    } else {
                        print("Фото не изменилось")
                    }
                    
                } catch {
                    print("Ошибка при чтении файла \(error)")
                    smthGoesWrong = true
                }
                
            } else {
                
                do {
                    try userData.write(to: path, options: .atomic)
                    print("Файл создается впервые")
                } catch {
                    print("Ошибка при записи файла \(error)")
                    smthGoesWrong = true
                }
            }
        }
        
        
    }
    
    
    
    func readDataFromFile() -> Data? {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(fileName)
            
            do {
                let data = try Data(contentsOf: path)
                return data
            } catch {
                print("Error here")
                smthGoesWrong = true
                return nil
            }
            
        }
        return nil
    }
    
    
    
}
