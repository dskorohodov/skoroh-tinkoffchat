//
//  userProfileSettings.swift
//  TinkoffChat
//
//  Created by Dmitry on 03/04/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit

class UserProfileSettings: NSObject, NSCoding {
    
    var userName: String?
    var textAboutMe: String?
    var userPhoto: UIImage?
    var textColor: UIColor?
    
    
    init(userName: String?, textAboutMe: String?, userPhoto: UIImage?, textColor: UIColor?) {
        self.userName = userName
        self.textAboutMe = textAboutMe
        self.userPhoto = userPhoto
        self.textColor = textColor
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        
        userName = aDecoder.decodeObject(forKey: "userName") as? String
        textAboutMe = aDecoder.decodeObject(forKey: "textAboutMe") as? String
        userPhoto = aDecoder.decodeObject(forKey: "userPhoto") as? UIImage
        textColor = aDecoder.decodeObject(forKey: "textColor") as? UIColor
    
    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userName, forKey: "userName")
        aCoder.encode(textAboutMe, forKey: "textAboutMe")
        aCoder.encode(userPhoto, forKey: "userPhoto")
        aCoder.encode(textColor, forKey: "textColor")
    }

}
