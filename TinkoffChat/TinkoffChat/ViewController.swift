//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Dmitry on 05/03/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    @IBOutlet weak var aboutMeTextView: UITextView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var buttonSaveWithGCD: UIButton!
    @IBOutlet weak var buttonSaveWithOperation: UIButton!
    
    
    let defaultImage = UIImage(named: "avatar")
    let imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        userNameTextField.delegate = self
        aboutMeTextView.delegate = self
        imagePicker.delegate = self
        self.activityIndicator.isHidden = true;
        
        //to recognize tap on image
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        userPhoto.isUserInteractionEnabled = true
        userPhoto.addGestureRecognizer(tapGesture)
        
        
        //inactive status for buttons
        self.buttonSaveWithGCD.isEnabled = false
        self.buttonSaveWithOperation.isEnabled = false
        
        
        loadDataFromCoreData()
        
        
        
    }
    
    
    func loadDataFromFileOperation() {
        
        let operationQueue = OperationQueue()
        operationQueue.qualityOfService = .userInitiated
        
        let newOperation = OperationDataManager(name: nil, aboutMe: nil, userPhoto: nil, textColor: nil, save: false)
        
        
        newOperation.completionBlock = { [weak self] in
            
            if (!newOperation.smthGoesWrong) {
                
                DispatchQueue.main.async {
                    self?.userNameTextField.text = newOperation.newSettings.userName
                    self?.aboutMeTextView.text = newOperation.newSettings.textAboutMe
                    self?.userPhoto.image = newOperation.newSettings.userPhoto
                    self?.aboutMeTextView.textColor = newOperation.newSettings.textColor
                }
                
                
                
            }
        }
        
        
        operationQueue.addOperation(newOperation)
    }
    
    
    
    
    func loadDataFromCoreData() {
        
        let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
        var fetchResult:[UserProfile] = []
        var userProfile: UserProfile
        
        do {
            fetchResult = try StorageManager.instance.saveContext!.fetch(fetchRequest)
        } catch {
            print("Error while retrieving entity \(error)")
        }
        
        guard fetchResult.count != 0 else {
            return
        }
        
        userProfile = fetchResult.last!
        
        DispatchQueue.main.async {
            self.userNameTextField.text = userProfile.userName
            self.aboutMeTextView.text = userProfile.shortInformation
            if (userProfile.userPhoto != nil) {
                self.userPhoto.image = UIImage(data: userProfile.userPhoto! as Data)
            }
            self.aboutMeTextView.textColor = userProfile.shortInfoTextColor as? UIColor
        }
        
    }
    
    
    
    
    
    @IBAction func userNameChanged(_ sender: Any) {
        print("User name changed")
        self.buttonSaveWithOperation.isEnabled = true
        self.buttonSaveWithGCD.isEnabled = true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("About me text changed")
        self.buttonSaveWithOperation.isEnabled = true
        self.buttonSaveWithGCD.isEnabled = true
    }
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func changeTextColor(_sender: UIButton) {
        self.aboutMeTextView.textColor = _sender.backgroundColor
        print("ПОМЕНЯЛСЯ ЦВЕТ")
        self.buttonSaveWithOperation.isEnabled = true
        self.buttonSaveWithGCD.isEnabled = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userNameTextField.resignFirstResponder()
        return true
        
    }
    
    // MARK: Photo to avatar
    
    func imageTapped() {
        
        
        
        let alert = UIAlertController(title: "Новая фотография", message: "Вы хотите обновить фотографию профиля?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let fromCamera = UIAlertAction(title: "С камеры", style: .default) {action -> Void in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let usePhotoLibrary = UIAlertAction(title: "Из альбома", style: .default) {action -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        let mekeDefault = UIAlertAction(title: "Вернуть фото по дефолту", style: .default) {action -> Void in
            self.userPhoto.image = self.defaultImage
        }
        
        alert.addAction(cancelAction);
        alert.addAction(fromCamera);
        alert.addAction(usePhotoLibrary)
        alert.addAction(mekeDefault)
        
        self.present(alert, animated: true, completion: nil);
        
        self.buttonSaveWithOperation.isEnabled = true
        self.buttonSaveWithGCD.isEnabled = true
        
        
    }
    
    
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.userPhoto.contentMode = .scaleToFill
            self.userPhoto.image = pickedImage
            
        } else if let pickedImage1 = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.userPhoto.contentMode = .scaleToFill
            self.userPhoto.image = pickedImage1
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    // MARK: Navigation
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    // MARK: Saving Data
    
    
    @IBAction func saveWithGCD(_ sender: Any?) {
        
        
        let gcd = GCDDataManager()
        startAnimate()
        gcd.writeWithGCD(name: self.userNameTextField.text, aboutMe: self.aboutMeTextView.text, userPhoto: userPhoto.image, textColor: self.aboutMeTextView.textColor, vc: self)
        
        
    }
    
    
    @IBAction func saveWithOperation(_ sender: Any?) {
        
        
        self.startAnimate()
        
        let operationQueue = OperationQueue()
        operationQueue.qualityOfService = .utility
        
        
        let newOperation = OperationDataManager(name: self.userNameTextField.text, aboutMe: self.aboutMeTextView.text, userPhoto: self.userPhoto.image, textColor: self.aboutMeTextView.textColor, save: true)
        
        
        newOperation.completionBlock = { [weak self] in
            
            DispatchQueue.main.async {
                
                self?.stopAnimate()
                
                self?.alertSuccessDataLoad()
                
            }
            
        }
        
        
        operationQueue.addOperation(newOperation)
        
        
    }
    
    @IBAction func saveWithCoreData(_ sender: Any) {
        
        let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
        var fetchResult:[UserProfile] = []
        var userProfile: UserProfile
        
        do {
            fetchResult = try StorageManager.instance.saveContext!.fetch(fetchRequest)
        } catch {
            print("Error while retrieving entity \(error)")
        }
        
        if fetchResult.count == 0 {
            userProfile = UserProfile()
        } else {
            userProfile = fetchResult.last!
            
            userProfile.userName = self.userNameTextField.text
            userProfile.shortInformation = self.aboutMeTextView.text
            userProfile.shortInfoTextColor = self.aboutMeTextView.textColor
            
            if let photoData = UIImageJPEGRepresentation(self.userPhoto.image!, 1.0) {
                userProfile.userPhoto = photoData as NSData
            }
            
        }
        
        StorageManager.instance.performSave(context: StorageManager.instance.saveContext!, completionHandler: {print("Success store userData")})
        
        
    }
    
    
    
    func alertSuccessDataLoad() {
        let alert = UIAlertController(title: nil, message: "Данные сохранены", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil);
        
    }
    
    
    func alertErrorDataLoad(GCD: Bool) {
        let alert = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let repeatAction = UIAlertAction(title: "Повторить", style: .default) {action -> Void in
            
            if (GCD) {
                self.saveWithGCD(nil)
            } else {
                self.saveWithOperation(nil)
            }
            
        }
        
        alert.addAction(okAction)
        alert.addAction(repeatAction)
        self.present(alert, animated: true, completion: nil);
        
        
    }
    
    
    
    
    
    func startAnimate() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        self.buttonSaveWithGCD.isEnabled = false
        self.buttonSaveWithOperation.isEnabled = false
        
    }
    
    func stopAnimate() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        
        self.buttonSaveWithGCD.isEnabled = false
        self.buttonSaveWithOperation.isEnabled = false
    }
    
    
}




// MARK: extension to store UIColor 

extension UIColor {
    class func color(withData data:Data) -> UIColor {
        return NSKeyedUnarchiver.unarchiveObject(with: data) as! UIColor
    }
    
    func encode() -> Data {
        return NSKeyedArchiver.archivedData(withRootObject:self)
    }
}










