//
//  StorageManager.swift
//  TinkoffChat
//
//  Created by Dmitry on 10/05/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStack: NSObject {
    
    //NSPersistentStore 
    //Для инициализации нужна лишь ссылка. Остальное сделает координатор
    
    private var storeURL: URL {
        get{
            let documentsDirURL : URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let url = documentsDirURL.appendingPathComponent("Store.sqlite")
            return url
        }
    }
    
    
    //NSManagedObjectModel
    //Необходимо указать файл модели 
    
    private let managedObjectModelName = "TinkoffChat"
    private var _managedObjectModel: NSManagedObjectModel?
    private var managedObjectModel: NSManagedObjectModel? {
        get{
            if _managedObjectModel == nil {
                guard let modelURL = Bundle.main.url(forResource: managedObjectModelName, withExtension: "momd") else {
                    print("Empty model url!")
                    return nil
                }
                
                _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
            
            }
            
            return _managedObjectModel
        }
    }
    
    
    // NSPersistentStoreCoordinator 
    
    private var _persistentStoreCoordinator : NSPersistentStoreCoordinator?
    private var persistentStoreCoordinator : NSPersistentStoreCoordinator? {
        get{
            if _persistentStoreCoordinator == nil {
                guard let model = self.managedObjectModel else {
                    print("Empty managed object model")
                    return nil
                }
                _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
                
                do {
                    try _persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
                } catch {
                    assert(false, "Error adding persistent store to coordinator: \(error)")
                }
            }
            return _persistentStoreCoordinator
        }
    }
    
    //NSManagedObjectContext (Master - общается с координатором и отвечает за сохранение данных в хранилище)
    
    private var _masterContex: NSManagedObjectContext?
    private var masterContex : NSManagedObjectContext? {
        get{
            if _masterContex == nil {
                let contex = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                guard let persistentStoreCoordinator = self.persistentStoreCoordinator else {
                    print("Empty persistent store coordinator!")
                    return nil
                }
                
                contex.persistentStoreCoordinator = persistentStoreCoordinator
                contex.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
                contex.undoManager = nil
                _masterContex = contex
                
            }
            return _masterContex
        }
    }
    
    //NSManagedObjectContext (Main - для отображения данных в UI)
    private var _mainContext: NSManagedObjectContext?
    private var mainContext : NSManagedObjectContext? {
        get{
            if _mainContext == nil {
                let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
                guard let parentContext = self.masterContex else {
                    print("No master context")
                    return nil
                }
                
                context.parent = parentContext
                context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
                context.undoManager = nil
                _mainContext = context
            }
            return _mainContext
        }
    }
    
    
    //NSManagedObjectContext (Save - для импорта данных)
    
    private var _saveContext : NSManagedObjectContext?
    public var saveContext: NSManagedObjectContext? {
        get{
            if _saveContext == nil {
                let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                
                guard let parentContext = self.mainContext else {
                    print("No master context")
                    return nil
                }
                
                context.parent = parentContext
                context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
                context.undoManager = nil
                _saveContext = context
            }
            
            return _saveContext
        }
    }
    
    
    
    //Сохранение контекстов 
    
    public func performSave(context: NSManagedObjectContext, completionHandler:(()-> Void)?) {
        if context.hasChanges {
            context.perform { [weak self] in
                do {
                    try context.save()
                }
                catch {
                    print("Context save error: \(error)")
                }
                
                if let parent = context.parent {
                    self?.performSave(context: parent, completionHandler: completionHandler)
                } else {
                    completionHandler?()
                }
                
            }
        } else {
            completionHandler?()
        }
    }
    
    
    
    
    deinit {
        print("Core data killed")
    }
    
    
    
    
    
    
    
    
    
    
    
    

}
