//
//  StorageManager.swift
//  TinkoffChat
//
//  Created by Dmitry on 14/05/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import UIKit
import CoreData

class StorageManager {
    
    static let instance = CoreDataStack()
    
    private init() {}
    
    class func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: StorageManager.instance.saveContext!)!
    }
    
}
