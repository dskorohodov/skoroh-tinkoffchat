//
//  UserProfile+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Dmitry on 16/05/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import Foundation
import CoreData


extension UserProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfile> {
        return NSFetchRequest<UserProfile>(entityName: "UserProfile")
    }

    @NSManaged public var shortInformation: String?
    @NSManaged public var userName: String?
    @NSManaged public var userPhoto: NSData?
    @NSManaged public var shortInfoTextColor: NSObject?

}
