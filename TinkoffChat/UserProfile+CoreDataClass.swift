//
//  UserProfile+CoreDataClass.swift
//  TinkoffChat
//
//  Created by Dmitry on 16/05/2017.
//  Copyright © 2017 DmitrySkorokhodov. All rights reserved.
//

import Foundation
import CoreData


public class UserProfile: NSManagedObject {

    convenience init() {
        self.init(entity: StorageManager.entityForName(entityName: "UserProfile"), insertInto: StorageManager.instance.saveContext)
    }
    
}
